//
//  FilteringList.swift
//  IKONTest
//
//  Created by Aji Prakosa on 14/03/24.
//

import SwiftUI

extension Binding {
    func onChange(_ handler: @escaping () -> Void) -> Binding<Value> {
        Binding(
            get: { self.wrappedValue },
            set: { newValue in
                self.wrappedValue = newValue
                handler()
            }
        )
    }
}

struct FilteringList<T: Identifiable, Content: View>: View {
    @State private var filteredItems = [T]()
    @State private var filterString = ""

    let listItems: [T]
    let filterKeyPaths: [KeyPath<T, String>]
    let content: (T) -> Content

    var body: some View {
        VStack {
            HStack(alignment: .top) {
              Image("search")
                .resizable()
                .frame(width: 30, height: 30)
                TextField("Type to filter", text: $filterString.onChange(applyFilter))
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.horizontal)
               }
            .padding()
            .overlay(
              RoundedRectangle(cornerRadius: 8)
                .stroke(Color.gray, lineWidth: 1)
            )
            .padding()
           List(filteredItems, rowContent: content)
                .onAppear(perform: applyFilter)
        }
    }

    init(_ data: [T], filterKeys: KeyPath<T, String>..., @ViewBuilder rowContent: @escaping (T) -> Content) {
        listItems = data
        filterKeyPaths = filterKeys
        content = rowContent
    }

    func applyFilter() {
        let cleanedFilter = filterString.trimmingCharacters(in: .whitespacesAndNewlines)

        if cleanedFilter.isEmpty {
            filteredItems = listItems
        } else {
            filteredItems = listItems.filter { element in
                filterKeyPaths.contains {
                  element[keyPath: $0]
                    .localizedCaseInsensitiveContains(cleanedFilter)
                }
            }
        }
    }
}
