//
//  IKONTestApp.swift
//  IKONTest
//
//  Created by mymac on 14/03/24.
//

import SwiftUI

@main
struct IKONTestApp: App {
    var body: some Scene {
        WindowGroup {
            TodoList()
        }
    }
}
