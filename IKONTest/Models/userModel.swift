//
//  userModel.swift
//  IKONTest
//
//  Created by Aji Prakosa on 14/03/24.
//

import Foundation

struct UserModel: Identifiable, Codable  {
    let userId: Int
    let id: Int
    let title: String
    let body: String
    
    
    enum APIError: Error{
        case invalidUrl, requestError, decodingError, statusNotOk
    }

    struct APIService {
        
        func getTodos() async throws -> [UserModel] {
            
            guard let url = URL(string:  "https://jsonplaceholder.typicode.com/posts") else{
                throw APIError.invalidUrl
            }
            
            guard let (data, response) = try? await URLSession.shared.data(from: url) else{
                throw APIError.requestError
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else{
                throw APIError.statusNotOk
            }
            
            guard let result = try? JSONDecoder().decode([UserModel].self, from: data) else {
                throw APIError.decodingError
            }
            
            return result
        }
    }
        
}
