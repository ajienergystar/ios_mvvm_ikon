//
//  DetailView.swift
//  IKONTest
//
//  Created by Aji Prakosa on 14/03/24.
//

import SwiftUI

struct DetailView: View {
    let item: UserModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 6) {
            Text("Detail Data")
                .font(.largeTitle)
                .padding(.bottom, 10)
            Text("User ID: \(item.userId)")
            Text("ID: \(item.id)")
            Text("Title: \(item.title)")
            Text("Body: \(item.body)")
            Spacer()
        }
        .padding(8)
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(item: UserModel(userId: 0, id: 0, title: "", body: ""))
    }
}
