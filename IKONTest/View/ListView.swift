//
//  ListView.swift
//  IKONTest
//
//  Created by Aji Prakosa on 14/03/24.
//

import SwiftUI

struct TodoList: View {
    @StateObject var vm = UserViewModel()
    @State private var searchText: String = ""
    @State private var value: String = ""
    @State private var show_modal = Bool()
    
    var body: some View {
        NavigationView {
            VStack {
                HStack(alignment: .top) {
                  Image("search")
                    .resizable()
                    .frame(width: 24, height: 24)
                  TextField("", text: $value)
                }
                .onTapGesture {
                    show_modal = true
                    UIView.setAnimationsEnabled(true)
                }
                .fullScreenCover(isPresented: $show_modal) {
                    SearchData()
                }
                .padding()
                .overlay(
                  RoundedRectangle(cornerRadius: 8)
                    .stroke(Color.gray, lineWidth: 1)
                )
                .padding()
                
                List{
                    ForEach(vm.userModel){user in
                        NavigationLink(destination: DetailView(item: user)) {
                            VStack(alignment: .leading, spacing: 6) {
                                Text("ID: \(user.id)")
                                Text("Title: \(user.title)")
                            }
                        }
                    }
                }
                .task {
                    await vm.getDatas()
                }
                .listStyle(.inset)
                .padding()
                .navigationTitle("Todo List")
                
            }
        }
    }
}
