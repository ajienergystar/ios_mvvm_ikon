//
//  SearchData.swift
//  IKONTest
//
//  Created by Aji Prakosa on 14/03/24.
//

import SwiftUI

struct SearchData: View {
    @StateObject var vm = UserViewModel()
    @State private var searchText: String = ""

    var body: some View {
        NavigationView {
            FilteringList(vm.userModel, filterKeys: \.title) { user in
                NavigationLink(destination: DetailView(item: user)) {
                    VStack(alignment: .leading, spacing: 6) {
                        Text("ID: \(user.id)")
                        Text("Title: \(user.title)")
                    }
                }
            }
            .task {
                await vm.getDatas()
            }
            .listStyle(.inset)
            .padding()
            .navigationTitle("Search Data")
        }
    }
}

struct SearchData_Previews: PreviewProvider {
    static var previews: some View {
        SearchData()
    }
}
