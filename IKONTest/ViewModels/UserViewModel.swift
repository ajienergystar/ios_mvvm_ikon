//
//  UserViewModel.swift
//  IKONTest
//
//  Created by Aji Prakosa on 14/03/24.
//

import Foundation

@MainActor
class UserViewModel: ObservableObject {
  
    @Published var userModel: [UserModel] = []
    @Published var errorMessage = ""
    @Published var hasError = false
    
    func getDatas() async {
        guard let data = try?  await UserModel.APIService().getTodos() else {
            self.userModel = []
            self.hasError = true
            self.errorMessage  = "Server Error"
            return
        }
            
        self.userModel = data
            
    }
}
